import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27, GPIO.OUT)

numberOfPresses = 0;

while True:
    input_state = GPIO.input(17)
    if input_state == False:
        print('Button Pressed')
        time.sleep(0.2)
        if numberOfPresses % 2 == 0:
            GPIO.output(27, False)
            print ('Off')
        else:
            GPIO.output(27, True)
            print('On')